package bj1

class Card(val value: Int, val suit: Int) { //construtor, getter

    //getter  - 2 equal ways
    //val suitName: String get() = "Spades"
    //val valueName: String get(){
    //    return "Ace"
    //}

    init { //complements the constructor
        //validations of the variables

        //val msg = "Bad suit"

        //fun msg(): String{
        //    return "Bad suit"
        //}

        //anonymous functions
        //val msg1 = fun() : String{
        //    return "Bad face"
        //}

        //val msg2 = fun() : String{
        //return "Bad suit"
        //}

        //still anonymous functions
        //val msg1 = fun() = "Bad face" //it knows it returns a string

        //val msg2 = fun() = "Bad suit" //it knows it returns a string

        //lambda expressions
        //val msg1 = { "Bad face" }
        //val msg2 = { "Bad suit" }

        //require(value in 1..13, msg1)
        //require(suit in 1..4, msg2) //return an illegal argument exception

        require(value in 1..13, { "Bad face $value" })
        require(suit in 1..4, { "Bad suit $suit" }) //return an illegal argument exception

        //check(suit in 1..4) //return an illegal state exception
        //assert(suit in 1..4)
    }


    val suitName: String
        get() = when (suit) {
            1 -> "Spades"
            2 -> "Hearts"
            3 -> "Clubs"
            4 -> "Diamonds"
            else -> throw IllegalStateException()
        }

    val valueName: String
        get() = when (value) {
            1 -> "Ace"
            in 2..10 -> value.toString()
            11 -> "Jack"
            12 -> "Queen"
            13 -> "King"
            else -> throw IllegalStateException()
        }

    val name: String get() = "$valueName of $suitName" //string interpolation
    //same as valueName + " of " + suitName

    val points: Int
        get() = when (value) {
            in 1..9 -> value
            in 10..13 -> 10
            else -> throw IllegalStateException()
        }
}

