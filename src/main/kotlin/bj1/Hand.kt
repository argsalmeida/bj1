package bj1

class Hand(val name: String) {

    //private val _cards: MutableList<Card> = mutableListOf<Card>()
    private val _cards = mutableListOf<Card>() // it infers the type

    val cards:List<Card> get() = _cards //to get the cards in the hand

    fun add(card: Card) {
        //same thing as throw UnsupportedOperationException("message") in java
        //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.

        _cards.add(card)
    }

    //val size: Int get()  = _cards.size
    val size get() = _cards.size //it infers is an Int

    val points1: Int
        get() {
            var t = 0
            for (c in _cards) t += c.points
            return t
        }

    val points2: Int get() {
        var t = 0
        //_cards.forEach({ t += it.points }) //if the last parameter is a lambda expression I can write it outside the parenthesis
        //_cards.forEach(){ t += it.points } //if there is only on parameter and its a lambda expression I can omit the parenthesis
        _cards.forEach { t += it.points }
        return t
    }

    //val points: Int get() {
    //    return _cards.sumBy { it.points }
    //}
    //val points: Int get() = _cards.sumBy { it.points }
    val points get() = _cards.sumBy { it.points }
}