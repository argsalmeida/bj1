package bj1

import org.junit.Test
import kotlin.test.assertEquals

class DeckTest {

    @Test
    fun t1() {

        val d1 = Deck(shuffle = false)
        //if I want to shuffle I can make just:
        //val d1 = Deck()
        assertEquals(52, d1.size)

        val c1 = d1.take()
        println(c1.name)
        assertEquals(51, d1.size)
        assertEquals("Ace of Spades", c1.name)

        val c2 = d1.take()
        println(c2.name)
        assertEquals(50, d1.size)
        assertEquals("2 of Spades", c2.name)

        println()

        renderDeck(d1)
    }

    private fun renderDeck(d1: Deck) {
        d1.cards.forEach { println(it.name) }
    }
}