package bj1

import org.junit.Test

class LambdaTest {

    @Test
    fun testAnonymousFunctions() {

        val a = fun() { //anonymous function () -> Unit, receives no arguments and return 'void'
            println(111)
        }

        /*
        val b = fun(p1: Int): Int {
            return p1 * p1
        }
        same thing as:
        */
        val b = fun(p1: Int) = p1 * p1


        // a()
        // println(b(3))

        useFunctions(a, b)
        //same thing as:
        useFunctions(fun() { println(111) }, fun(p1) = p1 * p1)

    }

    @Test
    fun testLambdaFunctions() {
        //lambda function no fun()
        val a = {
            println(111)
        }

        val b = { p1: Int -> p1 * p1 } // {arg -> return}

        val c = { p1: Int, p2: Int -> p1 * p2 }

        println(c(2, 3))

        useFunctions(a, b)

        //useFunctions({ println(111) }, { p1 -> p1 * p1 })
        //same as:
        useFunctions({ println(111) }, { it * it }) //'it' is a special name


    }

    fun useFunctions(f1: () -> Unit, f2: (Int) -> Int) {
        f1()
        println(f2(10))
    }
}
